#!/bin/bash

git -C /root/shared/$SUB_MODULE_NAME/ checkout -B temp-branch
git -C /root/wikidata push original master
git -C /root/shared/$SUB_MODULE_NAME/ checkout master