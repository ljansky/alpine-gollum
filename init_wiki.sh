#!/bin/sh
git remote add original /root/shared/$1/.git
git fetch original
git merge --allow-unrelated-histories original/master

/usr/bin/gollum /root/wikidata --config /root/config.rb