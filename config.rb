Gollum::Page.send :remove_const, :FORMAT_NAMES if defined? Gollum::Page::FORMAT_NAMES

require 'json'

wiki_options = {
  :live_preview => true,
  :allow_uploads => true,
  :per_page_uploads => true,
  :allow_editing => ENV['ALLOW_EDITING'],
  :css => true,
  :js => true,
  :mathjax => true,
  :h1_title => true
}
Precious::App.set(:wiki_options, wiki_options)

Gollum::Filter::PlantUML.configure do |config|
    config.url = ENV['PLANT_UML_PATH']
    config.verify_ssl = false
end

Gollum::Hook.register(:post_commit, :hook_id) do |committer, sha1|
        system('/root/wikidata/.git/hooks/post-commit')
end

module Precious
  class App < Sinatra::Base
    use Rack::Auth::Basic, "Restricted Area" do |username, password|
      [username, password] == [ENV['USERNAME'], ENV['PASSWORD']]
    end
  end
end

options = {
  :dummy_auth => false,
  # Specify committer name as just the user name
  :author_format => Proc.new { |user| user.name },
  # Specify committer e-mail as just the user e-mail
  :author_email => Proc.new { |user| user.email }
}